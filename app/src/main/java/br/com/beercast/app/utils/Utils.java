package br.com.beercast.app.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

/**
 * Created by danielideriba on 15/02/18.
 */

public class Utils {
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    public static final Gson GSON = new GsonBuilder()
            .setDateFormat(DEFAULT_DATE_FORMAT)
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    private Utils() { }

    public static String removeHtmlTags(String description) {
        return description.replaceAll("\\<[^>]*>","");
    }

    public static String milliSecondsToTimer(long milliseconds){
        String output = "00:00:00";
        long seconds = milliseconds / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        seconds = seconds % 60;
        minutes = minutes % 60;
        hours = hours % 60;

        String secondsD = String.valueOf(seconds);
        String minutesD = String.valueOf(minutes);
        String hoursD = String.valueOf(hours);

        if (seconds < 10)
            secondsD = "0" + seconds;
        if (minutes < 10)
            minutesD = "0" + minutes;
        if (hours < 10)
            hoursD = "0" + hours;

        output = hoursD + " : " + minutesD + " : " + secondsD;
        return output;
    }

    public static int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    public static String getAudioDuration(long millisDuration){
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millisDuration),
                TimeUnit.MILLISECONDS.toMinutes(millisDuration),
                TimeUnit.MILLISECONDS.toSeconds(millisDuration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisDuration))
        );
    }
}
