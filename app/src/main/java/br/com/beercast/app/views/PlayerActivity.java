package br.com.beercast.app.views;

import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import br.com.beercast.app.R;
import br.com.beercast.app.retrofit.ServerException;
import br.com.beercast.app.utils.Utils;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by danielideriba on 08/03/18.
 */

public class PlayerActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    @BindView(R.id.cover)
    ImageView cover;

    @BindView(R.id.description_player)
    TextView descriptionPlayer;

    @BindView(R.id.seekBarTestPlay)
    SeekBar seekBarProgress;

    @BindView(R.id.play_pause)
    ImageButton playPause;

    private MediaPlayer mediaPlayer;
    private int mediaFileLengthInMilliseconds;

    private String urlMp3;

    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        HashMap<String, String> playerData = (HashMap<String, String>)intent.getSerializableExtra("playerData");

        initSeekBar();
        initMediaPlayer();

        Picasso.with(this).load(playerData.get("fullimage")).into(cover);
        descriptionPlayer.setText(Utils.removeHtmlTags(playerData.get("description")));
        urlMp3 = playerData.get("audio");
    }

    private void initMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    private void initSeekBar() {
        seekBarProgress.setMax(99);
        seekBarProgress.setOnTouchListener(this);
    }

    private void primarySeekBarProgressUpdater() {
        seekBarProgress.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification,1000);
        }
    }

    @OnClick(R.id.play_pause)
    public void clickAndplay(View view) {
        try {
            mediaPlayer.setDataSource(urlMp3);
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL

        if(!mediaPlayer.isPlaying()){
            mediaPlayer.start();
            playPause.setImageResource(R.drawable.play_pause);
        } else {
            mediaPlayer.pause();
            playPause.setImageResource(R.drawable.pause);
        }

            primarySeekBarProgressUpdater();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.seekBarTestPlay){
            /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
            if(mediaPlayer.isPlaying()){
                SeekBar sb = (SeekBar)v;
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
                mediaPlayer.seekTo(playPositionInMillisecconds);
            }
        }
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        /** MediaPlayer onCompletion event handler. Method which calls then song playing is complete*/
        playPause.setImageResource(R.drawable.play_pause);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        /** Method which updates the SeekBar secondary progress by current song loading from URL position*/
        seekBarProgress.setSecondaryProgress(percent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mediaPlayer.stop();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_player;
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final ServerException exception) {
//        onServerError(exception, findViewById(android.R.id.content));
        Log.i("TAG", "--RES-"+exception.getMessage());
    }

    @Override
    public void onClick(View view) {

    }
}
