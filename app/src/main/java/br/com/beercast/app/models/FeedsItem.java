package br.com.beercast.app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by danielideriba on 01/02/18.
 */

public class FeedsItem {
    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("link")
    private String link;

    @SerializedName("url")
    private String url;

    @SerializedName("created")
    private String created;

    @SerializedName("postthumbnail")
    private String postthumbnail;

    @SerializedName("fullimage")
    private String fullimage;

    private List<Enclosures> enclosures;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getUrl() {
        return url;
    }

    public String getCreated() {
        return created;
    }

    public String getPostthumbnail() {
        return postthumbnail;
    }

    public String getFullimage() {
        return fullimage;
    }

    public List<Enclosures> getEnclosures() {
        return enclosures;
    }
}
