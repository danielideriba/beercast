package br.com.beercast.app.views;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import br.com.beercast.app.R;
import br.com.beercast.app.retrofit.ServerException;
import br.com.beercast.app.utils.Utils;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by danielideriba on 11/04/18.
 */

public class ExoPlayerActivity extends BaseActivity implements MediaController.MediaPlayerControl{

    private static final long TIME_DELAY_MILLIS = 300;

    @BindView(R.id.cover)
    ImageView cover;

    @BindView(R.id.description_player)
    TextView descriptionPlayer;

    @BindView(R.id.seekBarTestPlay)
    SeekBar seekBarProgress;

    @BindView(R.id.play_pause)
    ImageButton playPause;

    @BindView(R.id.play_backward)
    ImageButton playBackward;

    @BindView(R.id.play_forward)
    ImageButton playForward;

    @BindView(R.id.currentTimeSeekBar)
    TextView currentTimeSeekBar;

    @BindView(R.id.durationTimeSeekBar)
    TextView durationTimeSeekBar;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.volume)
    ImageButton volume;

    @BindView(R.id.exo_progress)
    DefaultTimeBar exoProgress;

    @BindView(R.id.exoPlayerControlView)
    PlayerControlView exoPlayerControlView;

    private Context context;
    private boolean mSlideState;
    private ActionBarDrawerToggle mDrawerToggle;
    private MediaPlayer mediaPlayer;
    private int mediaFileLengthInMilliseconds;
    private AudioManager audioManager;
    private String urlMp3;
    private DefaultBandwidthMeter defaultBandwidthMeter;
    private DataSource.Factory dataSourceFactory;
    private boolean durationSet = false;
    private boolean volumeDown = false;

    private SimpleExoPlayer exoPlayer;

    private final Handler handler = new Handler();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        mSlideState = false;
        Intent intent = getIntent();
        HashMap<String, String> playerData = (HashMap<String, String>)intent.getSerializableExtra("playerData");

        setDrawerToggle(mToolbar, mDrawerLayout, navigationView);

        Picasso.with(this).load(playerData.get("fullimage")).into(cover);
        descriptionPlayer.setText(Utils.removeHtmlTags(playerData.get("description")));
        urlMp3 = playerData.get("audio");

        initExoPLayer(urlMp3);
    }

    private void initExoPLayer(String urlMp3) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        defaultBandwidthMeter = new DefaultBandwidthMeter();

        dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "mediaPlayerSample"), defaultBandwidthMeter);

        Handler mainHandler = new Handler();
        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(urlMp3),
                dataSourceFactory,
                extractorsFactory, mainHandler, null);


        exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        exoPlayer.prepare(mediaSource);
        exoPlayer.addListener(new PlayerEventListener());

        //Debug exoplayer
        EventLogger eventLogger = new EventLogger(trackSelector);
        exoPlayer.addListener(eventLogger);
        exoPlayer.addMetadataOutput(eventLogger);
        exoPlayer.addAudioDebugListener(eventLogger);
        //Debug exoplayer

        exoPlayerControlView.setPlayer(exoPlayer);
        exoPlayerControlView.show();
        exoPlayerControlView.setShowTimeoutMs(0);
    }

    @OnClick(R.id.play_pause)
    public void clickAndplay(View view) {
        if(isPlaying()){
            removeTimeHandler();
            pause();
            playPause.setImageResource(R.drawable.play_player);
        } else {
            setCurrentTimeHandler();
            start();
            playPause.setImageResource(R.drawable.pause);
        }
    }

    @OnClick(R.id.volume)
    public void clickVolumeUpDown(View view){
        if(isPlaying()) {
            if (!volumeDown) {
                exoPlayer.setVolume(0.0f);
                volume.setBackgroundResource(0);
                volume.setImageResource(0);
                volume.setBackgroundResource(R.drawable.mute);
                volumeDown = true;
            } else {
                exoPlayer.setVolume(1.0f);
                volume.setImageResource(0);
                volume.setBackgroundResource(0);
                volume.setBackgroundResource(R.drawable.volumes);
                volumeDown = false;
            }
        }
    }


    private void setCurrentTimeHandler(){
        handler.postDelayed(
                new Runnable() {
                    public void run() {
                        currentTimeSeekBar.setText(Utils.getAudioDuration(exoPlayer.getCurrentPosition()));
                        handler.postDelayed(this, TIME_DELAY_MILLIS);
                    }
                },
                TIME_DELAY_MILLIS);
    }

    private void removeTimeHandler(){
        handler.removeCallbacksAndMessages(null);
    }

    @OnClick(R.id.play_forward)
    public void clickAndForward(View view) {
        exoPlayer.seekTo(exoPlayer.getCurrentPosition() + 10000);
    }

    @OnClick(R.id.play_backward)
    public void clickAndBackward(View view) {
        exoPlayer.seekTo(exoPlayer.getCurrentPosition() - 10000);
    }

    @Override
    public void start() {
        currentTimeSeekBar.setText(Utils.getAudioDuration(exoPlayer.getCurrentPosition()));
        exoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void pause() {
        exoPlayer.setPlayWhenReady(false);
    }

    @Override
    public int getDuration() {
        return (int) exoPlayer.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return (int) exoPlayer.getCurrentPosition();
    }

    @Override
    public void seekTo(int timeMillis) {
        long seekPosition =  Math.min(Math.max(0, timeMillis), getDuration());
        exoPlayer.seekTo(seekPosition);
    }

    @Override
    public boolean isPlaying() {
        return exoPlayer.getPlayWhenReady();
    }

    @Override
    public int getBufferPercentage() {
        return exoPlayer.getBufferedPercentage();
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_exoplayer;
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final ServerException exception) {
//        onServerError(exception, findViewById(android.R.id.content));
        Log.i("TAG", "--RES-"+exception.getMessage());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exoPlayer.setPlayWhenReady(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exoPlayer.setPlayWhenReady(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        exoPlayer.setPlayWhenReady(false);
    }

    //Progress listener
    public class ScrubListener implements DefaultTimeBar.OnScrubListener {

        @Override
        public void onScrubStart(TimeBar timeBar, long position) {
            Log.i("TAG", "Start--"+position);
        }

        @Override
        public void onScrubMove(TimeBar timeBar, long position) {
            Log.i("TAG", "Move--"+position);
        }

        @Override
        public void onScrubStop(TimeBar timeBar, long position, boolean canceled) {
            Log.i("TAG", "Stop--"+position);
        }
    }
    //Listener
    public class PlayerEventListener implements Player.EventListener {

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
           // Log.i("TAG", "--RES loading-"+isLoading);
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playbackState == Player.STATE_READY && !durationSet) {
                durationSet = true;
                durationTimeSeekBar.setText(Utils.getAudioDuration(exoPlayer.getDuration()));
                currentTimeSeekBar.setText(Utils.getAudioDuration(exoPlayer.getCurrentPosition()));
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {
            Log.i("TAG", "---cur repeat--"+Utils.getAudioDuration(exoPlayer.getCurrentPosition()));
        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
            currentTimeSeekBar.setText(Utils.getAudioDuration(exoPlayer.getCurrentPosition()));
            Log.i("TAG", "---cur Discontinuity--"+reason);

        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {
            Log.i("TAG", "---cur seek");
        }
    }
}
