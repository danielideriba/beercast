package br.com.beercast.app.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;

import br.com.beercast.app.R;
import br.com.beercast.app.retrofit.ServerException;
import butterknife.BindView;

/**
 * Created by danielideriba on 20/03/18.
 */

public class PubsActivity extends BaseActivity{

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.activity_title)
    TextView activityTitle;

    @BindView(R.id.list_error)
    LinearLayout listError;

    private Context context;
    private boolean mSlideState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        mSlideState = false;

        //Basic activity configs
        this.context = this;
        mSlideState = false;
        setActivityTitle(activityTitle, getResources().getString(R.string.activity_name_pubs));

        setDrawerToggle(mToolbar, mDrawerLayout, navigationView);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_pubs;
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final ServerException exception) {
//        onServerError(exception, findViewById(android.R.id.content));
        Log.i("TAG", "--RES-"+exception.getMessage());
    }
}

