package br.com.beercast.app.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.BinderThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import br.com.beercast.app.R;
import br.com.beercast.app.models.FeedsItem;
import br.com.beercast.app.utils.Utils;
import br.com.beercast.app.views.ExoPlayerActivity;
import br.com.beercast.app.views.MainActivity;
import br.com.beercast.app.views.PlayerActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by danielideriba on 16/02/18.
 */

public class FeedsAdapter extends BaseAdapter{

    private LayoutInflater inflater = null;

    private final List<FeedsItem> feedsItems;
    private final Context context;

    public FeedsAdapter(List<FeedsItem> feedsItems, Context context) {
        inflater = LayoutInflater.from(context);
        this.feedsItems = feedsItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return feedsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static final class ViewHolder {
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.full_title)
        TextView title;
        @BindView(R.id.description)
        TextView description;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.list_items, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.title.setTypeface(Typeface.createFromAsset(context.getAssets(),"BebasNeue.otf"),Typeface.BOLD);
        holder.description.setTypeface(Typeface.createFromAsset(context.getAssets(), "HelveticaNeue.otf"), Typeface.NORMAL);


        Picasso
                .with(context)
                .load(feedsItems.get(position).getPostthumbnail())
                .placeholder(R.drawable.loading)
                .error(R.drawable.cloud_error)
                .into(holder.icon);

        holder.title.setText(feedsItems.get(position).getTitle());
        holder.description.setText(Utils.removeHtmlTags(feedsItems.get(position).getDescription()));

        HashMap<String, String> extra = new HashMap<String, String>();
        extra.put("title", feedsItems.get(position).getTitle());
        extra.put("link",  feedsItems.get(position).getLink());
        extra.put("dateCreated",  feedsItems.get(position).getCreated());
        extra.put("description",  feedsItems.get(position).getDescription());
        extra.put("postThumbnail",  feedsItems.get(position).getPostthumbnail());
        extra.put("fullimage",  feedsItems.get(position).getFullimage());
        extra.put("url",  feedsItems.get(position).getUrl());
        extra.put("audio", feedsItems.get(position).getEnclosures().get(0).getUrlmp3());
        extra.put("typeMp3", feedsItems.get(position).getEnclosures().get(0).getType());
        extra.put("typeLength", feedsItems.get(position).getEnclosures().get(0).getLength());

        setListClickActions(view, position, extra);

        return view;
    }

    private void setListClickActions(View view, final int position, final HashMap<String, String> extras){
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ExoPlayerActivity.class);
                intent.putExtra("playerData", extras);
                context.startActivity(intent);
            }
        });
    }
}