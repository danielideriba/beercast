package br.com.beercast.app.interfaces;

import br.com.beercast.app.models.Feeds;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by danielideriba on 15/02/18.
 */

public interface Services {
    @GET("programas")
    Call<Feeds> getFeeds();
}