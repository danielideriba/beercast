package br.com.beercast.app.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by danielideriba on 01/02/18.
 */

public class Enclosures {
    @SerializedName("audio")
    private String urlmp3;

    @SerializedName("length")
    private String length;

    @SerializedName("type")
    private String type;

    public String getUrlmp3() {
        return urlmp3;
    }

    public String getLength() {
        return length;
    }

    public String getType() {
        return type;
    }
}
