package br.com.beercast.app.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;


import br.com.beercast.app.R;
import br.com.beercast.app.models.Feeds;
import br.com.beercast.app.retrofit.ListFeeds;
import br.com.beercast.app.retrofit.ServerException;
import br.com.beercast.app.views.adapters.FeedsAdapter;
import butterknife.BindView;

public class MainActivity extends BaseActivity{

    private Context context;
    private boolean mSlideState;
    private ProgressDialog progressDialog;

    @BindView(R.id.recipe_list_view)
    ListView recipeListView;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.activity_title)
    TextView activityTitle;

    @BindView(R.id.list_error)
    LinearLayout listError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Basic activity configs
        this.context = this;
        mSlideState = false;
        setActivityTitle(activityTitle, getResources().getString(R.string.activity_name));

        setDrawerToggle(mToolbar, mDrawerLayout, navigationView);

        progressDialog = initLoading(context);
        fetchListView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    protected void fetchListView(){
        ListFeeds listFeeds = new ListFeeds();
        listFeeds.loadFeeds(this);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final Feeds response) {
        feedsFetched(response);
        progressDialog.dismiss();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final ServerException exception) {
//        onServerError(exception, findViewById(android.R.id.content));
        Log.i("TAG", "--RES-"+exception.getMessage());
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final String strError){
        recipeListView.setVisibility(View.GONE);
        listError.setVisibility(View.VISIBLE);
    }

    public void feedsFetched(Feeds response) {
        FeedsAdapter adapter = new FeedsAdapter(response.getItems(), context);
        recipeListView.setAdapter(adapter);
    }

    private ProgressDialog initLoading(final Context context) {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMax(100);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("ProgressDialog bar example");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        return progressDialog;

    }

}
