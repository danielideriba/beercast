package br.com.beercast.app.retrofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import br.com.beercast.app.BuildConfig;
import br.com.beercast.app.interactors.BaseInteractor;
import br.com.beercast.app.interfaces.Services;
import br.com.beercast.app.models.Feeds;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by danielideriba on 15/02/18.
 */

public class ListFeeds extends BaseInteractor{

    private Services mService;

    public void loadFeeds(final Context context){
        Services mService = RetrofitClient.getServices();

        mService.getFeeds().enqueue(new Callback<Feeds>() {
            @Override
            public void onResponse(Call<Feeds> call, Response<Feeds> response) {
                if(response.isSuccessful()){
                    getEventBus().post(response.body());
                } else {
                    getEventBus().post(response.code());
                }
            }
            @Override
            public void onFailure(Call<Feeds> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    getEventBus().post("[404] Erro na autenticação");
                }
            }
        });
    }
}
