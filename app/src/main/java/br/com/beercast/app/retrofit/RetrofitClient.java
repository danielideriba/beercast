package br.com.beercast.app.retrofit;

import java.util.concurrent.TimeUnit;

import br.com.beercast.app.BuildConfig;
import br.com.beercast.app.interfaces.Services;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by danielideriba on 15/02/18.
 */

public class RetrofitClient {
    private static Retrofit retrofit = null;

    private static RetrofitClient instance = null;

    public static RetrofitClient getInstance() {
        return instance;
    }

    protected RetrofitClient(){
        getClient(BuildConfig.BASE_URL);
    }

    public static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(makeHttpClient(loggingInterceptor()))
                    .build();
        }
        return retrofit;
    }

    private static HttpLoggingInterceptor loggingInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if(BuildConfig.DEBUG){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        return interceptor;
    }

    private static OkHttpClient makeHttpClient(HttpLoggingInterceptor interceptor) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor())
                .build();

        return client;
    }

    public static Services getServices() {
        return RetrofitClient.getClient(BuildConfig.BASE_URL).create(Services.class);
    }
}
