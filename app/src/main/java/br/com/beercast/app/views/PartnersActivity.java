package br.com.beercast.app.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;

import org.greenrobot.eventbus.Subscribe;

import br.com.beercast.app.R;
import br.com.beercast.app.retrofit.ServerException;

/**
 * Created by danielideriba on 20/03/18.
 */

public class PartnersActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private Context context;
    private String ACTIVITY_NAME = "";

    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mSlideState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        mSlideState = false;

        ACTIVITY_NAME = getResources().getString(R.string.activity_name_partners);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_partners;
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final ServerException exception) {
//        onServerError(exception, findViewById(android.R.id.content));
        Log.i("TAG", "--RES-"+exception.getMessage());
    }
}
