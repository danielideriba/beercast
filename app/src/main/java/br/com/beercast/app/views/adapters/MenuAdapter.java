package br.com.beercast.app.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import br.com.beercast.app.R;


/**
 * Created by danielideriba on 04/03/18.
 */

public class MenuAdapter extends BaseAdapter {

    private LayoutInflater inflater = null;
    private final Context context;

    public MenuAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.nav_header_main, parent, false);
        return view;
    }
}