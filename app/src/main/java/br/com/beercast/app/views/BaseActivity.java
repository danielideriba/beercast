package br.com.beercast.app.views;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import br.com.beercast.app.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by danielideriba on 15/02/18.
 */

public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Context context;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mSlideState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        registerBus();
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
        mSlideState = false;
    }

    private void registerBus() {
        if (!getEventBus().isRegistered(this))
            getEventBus().register(this);
    }

    public EventBus getEventBus() {
        return EventBus.getDefault();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBus();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_settings_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (getEventBus().isRegistered(this))
            getEventBus().unregister(this);
    }

    public void setDrawerToggle(Toolbar mToolbar, final DrawerLayout mDrawerLayout, NavigationView navigationView){
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mSlideState = false;

            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mSlideState = true;
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.menu);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        setToolbar(mToolbar);
        setNavigationView(navigationView);
        setNavigationClick(mToolbar, mDrawerLayout);
    }

    public void setToolbar(Toolbar mToolbar){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo_beercast);
    }

    public void setNavigationView(NavigationView navigationView){
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void setNavigationClick(Toolbar mToolbar, final DrawerLayout mDrawerLayout){
        mToolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mSlideState) {
                            mDrawerLayout.closeDrawer(Gravity.START);
                        } else {
                            mDrawerLayout.openDrawer(Gravity.START);
                        }
                    }
                });
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void setActivityTitle(TextView activityTitle, String activityName) {
        activityTitle.setText(activityName);
        activityTitle.setTypeface(Typeface.createFromAsset(context.getAssets(),"BebasNeue.otf"),Typeface.BOLD);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_eps:
                openActivity(MainActivity.class);
                return true;

            case R.id.nav_ep_baixados:
                openActivity(DownloadsActivity.class);
                return true;

            case R.id.nav_list_bares:
                openActivity(PubsActivity.class);
                return true;

            case R.id.nav_descontos:
                openActivity(Deals.class);
                return true;

            case R.id.nav_near_pubs:
                openActivity(MapActivity.class);
                return true;

            case R.id.nav_contactus:
                openActivity(ContactUsActivity.class);
                return true;

            case R.id.nav_patron_area:
                openActivity(PatronArea.class);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void openActivity(final Class c) {
        Intent intent = new Intent(this, c);
        startActivity(intent);
    }

    public void openActivityWithFlags(final Class c) {
        Intent intent = new Intent(this, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void openActivityForResult(Class c) {
        Intent intent = new Intent(this, c);
        startActivityForResult(intent, 100);
    }


    protected abstract int getLayoutResource();
}