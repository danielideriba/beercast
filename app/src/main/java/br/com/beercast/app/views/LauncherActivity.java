package br.com.beercast.app.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.greenrobot.eventbus.Subscribe;

import br.com.beercast.app.R;
import br.com.beercast.app.retrofit.ServerException;

/**
 * Created by danielideriba on 06/03/18.
 */

public class LauncherActivity extends BaseActivity  {

    private static final long DELAY_MILLIS = 2000;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        this.context = this;

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                openActivityWithFlags(MainActivity.class);
                finish();
            }
        }, DELAY_MILLIS);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_launcher;
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final ServerException exception) {
//        onServerError(exception, findViewById(android.R.id.content));
    }
}
