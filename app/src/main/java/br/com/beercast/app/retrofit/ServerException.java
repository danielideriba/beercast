package br.com.beercast.app.retrofit;

/**
 * Created by danielideriba on 15/02/18.
 */

public class ServerException extends BaseException {

    private int statusCode;

    public ServerException(final int statusCode, String detailMessage) {
        super(detailMessage);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
