package br.com.beercast.app.retrofit;

/**
 * Created by danielideriba on 15/02/18.
 */

public class BaseException extends Exception {
    public BaseException(String detailMessage) {
        super(detailMessage);
    }
}
