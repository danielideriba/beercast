package br.com.beercast.app;

import android.app.Application;

/**
 * Created by danielideriba on 15/02/18.
 */

public class CurrentApplication extends Application {

    private static CurrentApplication application;

    public void onCreate() {
        super.onCreate();
        application = this;
    }

    public static CurrentApplication getCurrentApplication() {
        return application;
    }
}