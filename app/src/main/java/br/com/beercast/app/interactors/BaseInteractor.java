package br.com.beercast.app.interactors;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import br.com.beercast.app.R;
import br.com.beercast.app.interfaces.Services;
import br.com.beercast.app.retrofit.RetrofitClient;
import br.com.beercast.app.retrofit.ServerException;

import static br.com.beercast.app.CurrentApplication.getCurrentApplication;

/**
 * Created by danielideriba on 15/02/18.
 */

public class BaseInteractor {

    public Services services() {
        return (Services) RetrofitClient.getInstance();
    }

    public boolean isSuccess(final retrofit2.Response response) {
        if (!response.isSuccessful()) {
            switch (response.code()) {
                case 403:
                    Log.e("TAG", "403");
                    getEventBus().post(getCurrentApplication().getString(R.string.unauthorized_error_message));
                    break;
                case 401:
                    Log.e("TAG", "401");
                    getEventBus().post(getCurrentApplication().getString(R.string.password_error_message));
                    break;
                case 400:
                    Log.e("TAG", "400");
                    break;
                default:
                    getEventBus().post(new ServerException(response.code(), getCurrentApplication().getString(R.string.server_error_message)));

            }
        }
        return response.isSuccessful();
    }


    public EventBus getEventBus() {
        return EventBus.getDefault();
    }

    void handleError(final String error) {
        getEventBus().post(new ServerException(-1, error));
    }
}